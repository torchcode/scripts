import ConfigParser, mmap

config_file = "/usr/share/applications/google-chrome.desktop"
add_string_to_each_section = ["StartupWMClass", "Google-chrome-stable"]
option = add_string_to_each_section[0]
value = add_string_to_each_section[1]


class Fixer:
    def check(self, cf, option, value):
        f = open(cf)
        s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
        if s.find(option+" = "+value) != -1:
            print "already fixed"
            exit(1)

    def fix(self, cf, option, value):
        Config = ConfigParser.ConfigParser()
        Config.optionxform=str
        Config.read(cf)

        cfgfile = open(cf,'w')

        for section in range(len(Config.sections())):
            Config.set(Config.sections()[section], option, value)

        Config.write(cfgfile)
        cfgfile.close()


if __name__ == "__main__":
    F = Fixer()
    F.check(config_file, option, value)
    F.fix(config_file, option, value)